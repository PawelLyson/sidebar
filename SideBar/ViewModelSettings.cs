﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace SideBar
{
    class ViewModelSettings : INotifyPropertyChanged
    {
        //private readonly SshCommands ModelPingCommand;
        private ViewSettings SettingsView;
        public bool IsOpenedWindow { get; private set; } = false;

        public ICommand PasswordChangedCommand { get; private set; }
        public ICommand SaveDataCommand { get; private set; }
        public ICommand CloseCommand { get; private set; }

        public ViewModelSettings()//(SshCommands modelPingCommand)
        {
            //ModelPingCommand = modelPingCommand;
            PasswordChangedCommand = new RelayCommand((object sender) => SshPassword = ((PasswordBox)sender).Password);
            SaveDataCommand = new RelayCommand((object sender) => SaveData());
            CloseCommand = new RelayCommand((object sender) => SettingsView.Close());
            LoadData();
        }

        private void LoadData()
        {
            SshUsername = SettingsModule.Ssh.Username;
            SshPassword = SettingsModule.Ssh.Password;
            SshPort = SettingsModule.Ssh.Port;
            SshIP = SettingsModule.Ssh.IpAddress;
        }

        private void SaveData()
        {
            SettingsModule.Ssh.Username = SshUsername;
            SettingsModule.Ssh.Password = SshPassword;
            SettingsModule.Ssh.Port = SshPort;
            SettingsModule.Ssh.IpAddress = SshIP;
            SettingsModule.SaveSettings();
        }

        public void ShowSettingsWindows()
        {
            SettingsView = new ViewSettings
            {
                DataContext = this
            };
            SettingsView.Show();
            IsOpenedWindow = true;
            SettingsView.Closed += SettingsWindowsCloseEvent;
            SettingsView.SshPassword.Password = SshPassword;
        }

        private void SettingsWindowsCloseEvent(object sender, EventArgs e)
        {
            SettingsView.Closed -= SettingsWindowsCloseEvent;
            IsOpenedWindow = false;
        }

        private string UserNameValue = "";
        public string SshUsername
        {
            get
            {
                return UserNameValue;
            }
            set
            {
                UserNameValue = value;
                NotifyPropertyChanged();
            }
        }
        private string SshPasswordValue = "";
        public string SshPassword
        {
            get
            {
                return SshPasswordValue;
            }
            set
            {
                SshPasswordValue = value;
                NotifyPropertyChanged();
            }
        }

        private string FullIPAddressValue = "";
        public string FullIPAddress
        {
            get
            {
                return FullIPAddressValue;
            }
            set
            {
                FullIPAddressValue = value;
                NotifyPropertyChanged();
            }
        }
        public ushort SshPort
        {
            get
            {
                return FullIPAddress.IndexOf(':') == -1 ? (ushort)22 : ushort.Parse(FullIPAddress.Split(':')[1]);
            }
            set
            {
                FullIPAddress = SshIP + ":" + value;
            }
        }

        public string SshIP
        {
            get
            {
                return FullIPAddress.IndexOf(':') == -1 ? FullIPAddress : FullIPAddress.Split(':')[0];
            }
            set
            {
                FullIPAddress = value + ":" + SshPort;
            }
        }

        void PasswordChangedHandler(Object sender, System.Windows.RoutedEventArgs args)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
