﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SideBar
{
    public static class SettingsModule
    {
        private static readonly string SettingsFileName = "Settings.bin";
        private static AllSettings settings;

        static SettingsModule()
        {
            settings = new AllSettings(); //TODO: Plik ustawień powinien być zaszyfrowany (przechowywanie hasła).
            LoadSettings();
        }

        public static SshSettings Ssh
        {
            get
            {
                return settings.ssh;
            }
        }

        public static PingSettings Ping
        {
            get
            {
                return settings.ping;
            }
        }


        public static void SaveSettings()
        {
            using (Stream fileSettingsStream = File.Create(SettingsFileName))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fileSettingsStream, settings);
            }
        }
        public static void LoadSettings()
        {
            if (File.Exists(SettingsFileName))
            {
                Debug.WriteLine("File exist");
                using (Stream fileSettingsStream = File.OpenRead(SettingsFileName))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    settings = (AllSettings)formatter.Deserialize(fileSettingsStream);
                }
            }
            else
            {
                Debug.WriteLine("No settings file exist");
            }
        }
    }

    [Serializable]
    class AllSettings
    {
        public SshSettings ssh = new SshSettings();
        public PingSettings ping = new PingSettings();
    }

    [Serializable]
    public class SshSettings
    {
        public string IpAddress { get; set; }
        public ushort Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    [Serializable]
    public class PingSettings
    {
        public bool FastPing { get; set; }
        public string ReplayFormat { get; set; }
    }
}
