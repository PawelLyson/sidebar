﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SideBar
{
    public class ViewModelMainWindow : INotifyPropertyChanged
    {
        private SshCommands ModelPingCommand = new SshCommands();
        private SshCommands SshSingleCommand = new SshCommands();
        private ViewModelSettings SettingsWindow;
        private ViewMainWindow ViewModel;
        
        public ICommand PingStartCommand { get; private set; }
        public ICommand StickCommand { get; private set; }
        public ICommand SettingsCommand { get; private set; }
        //public ICommand PortCheckCommand { get; private set; }

        public ViewModelMainWindow(ViewMainWindow viewMain)
        {
            ViewModel = viewMain;
            SettingsWindow = new ViewModelSettings();//(ModelPingCommand);

            StickCommand = new RelayCommand(new Action<object>(AppBarButtonClick));
            PingStartCommand = new RelayCommand(new Action<object>(StartPingButtonClick));
            SettingsCommand = new RelayCommand(new Action<object>(OpenSettingsWindowClick));
            //PortCheckCommand = new RelayCommand(new Action<object>(PortCheckClick));

            ModelPingCommand.AddEventListener(PingResultReachedResult);
        }

        private void AppBarButtonClick(object sender)
        {
            ViewModel.ToggleAppBarMode();
            if (StickButtonName == "Odepnij") StickButtonName = "Przypnij";
            else StickButtonName = "Odepnij";
        }

        private void StartPingButtonClick(object sender)
        {
            if (ModelPingCommand.IsRunningInfinite)
            {
                ModelPingCommand.StopInfiniteCommand();
            }
            else
            {
                PingResultString = "";
                ModelPingCommand.StartInfiniteCommand("ping -O -n " + IpAddressString); //TODO: Przenieśc to do Modelu, ViewModel nie powinien zarządzać poleceniami.
                string commandResult = SshSingleCommand.RunCommand("nmap -oX - -sT " + IpAddressString); //TODO: Oczekiwanie na wykonwanie komendy blokuje program. Poprawić.
                PortListResult = NmapXml.PortCheckToList(commandResult);
            }
        }

        private void OpenSettingsWindowClick(object sender)
        {
            if (!SettingsWindow.IsOpenedWindow)
            {
                SettingsWindow.ShowSettingsWindows();
            }
        }
        
        private void PortCheckClick(object sender) //TODO: metoda nie używana. Skasować ??
        {
            string commandResult = SshSingleCommand.RunCommand("nmap -oX - -sT " + IpAddressString); //TODO: Oczekiwanie na wykonwanie komendy blokuje program. Poprawić.
            PortListResult = NmapXml.PortCheckToList(commandResult);
        }

        void PingResultReachedResult(object sender, ResponseEventArgs e)
        {
            PingResultString += e.CommandResult + Environment.NewLine;
            //Debug.WriteLine(e.CommandResult);
        }

        private string PingResultStringValue;
        public string PingResultString
        {
            get
            {
                return PingResultStringValue;
            }
            set
            {
                PingResultStringValue = value;
                if (PingResultStringValue.Split('\n').Length > 500)
                {
                    int test = PingResultStringValue.IndexOf("\r\n"); //TODO: Do poprawy. Usuwa tylko pierwszy eleent przy kazdym dodaniu. POwinno miec detekcje znaku końca lini.
                    PingResultStringValue = PingResultStringValue.Remove(0, PingResultStringValue.IndexOf("\r\n")+2);
                }
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<ServerPortInfo> PortListResultValue;
        public ObservableCollection<ServerPortInfo> PortListResult
        {
            get
            {
                return PortListResultValue;
            }
            set
            {
                PortListResultValue = value;
                NotifyPropertyChanged();
            }
        }

        private string IpAddressStringValue;
        public string IpAddressString
        {
            get
            {
                return IpAddressStringValue;
            }
            set
            {
                IpAddressStringValue = value;
                NotifyPropertyChanged();
            }
        }

        private string StickButtonNameValue = "Odepnij";
        public string StickButtonName
        {
            get
            {
                return StickButtonNameValue;
            }
            set
            {
                StickButtonNameValue = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Action<object> execute;
        private readonly Func<object, bool> canExecute;
        //private event EventHandler CanExecuteChangedInternal;

        public RelayCommand(Action<object> execute)
            : this(execute, DefaultCanExecute)
        {
        }
        public RelayCommand(Action<object> execute, Func<object,bool> canExecute)
        {
            this.execute = execute ?? throw new ArgumentNullException("execute");
            this.canExecute = canExecute ?? throw new ArgumentNullException("canExecute");
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                //this.CanExecuteChangedInternal += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
                //this.CanExecuteChangedInternal -= value;
            }
        }
        public bool CanExecute(object parameter)
        {
            return this.canExecute != null && this.canExecute(parameter);
        }
        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
        private static bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}
