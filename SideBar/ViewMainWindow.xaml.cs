﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAppBar;

namespace SideBar
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class ViewMainWindow : Window
    {
        public ViewMainWindow()
        {
            InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            ToggleAppBarMode();
        }

        private void MainWrapPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            AppBarFunctions.SetAppBar(this, ABEdge.None);
        }

        private bool IsAppBarMode = false;
        public void ToggleAppBarMode()
        {
            if (IsAppBarMode)
            {
                AppBarFunctions.SetAppBar(this, ABEdge.None);
            }
            else
            {
                AppBarFunctions.SetAppBar(this, ABEdge.Left, MainWrapPanel);
            }
            IsAppBarMode = !IsAppBarMode;
        }

        private void ResultTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox result = (TextBox)sender;
            result.ScrollToEnd();
        }
    }
}
