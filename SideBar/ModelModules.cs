﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Renci.SshNet;

namespace SideBar
{
    class SshCommands
    {
        private ConnectionInfo ConnectionDetails;
        private SshClient SshClientConnection;
        private SshCommand SshRunningCommand;
        private Thread AsyncSshThread;
        private event EventHandler<ResponseEventArgs> ResponseEvent;
        public bool IsRunningInfinite { get; private set; } = false;

        private void ConnectToServer()
        {
            string ipAddress = SettingsModule.Ssh.IpAddress;
            ushort port = SettingsModule.Ssh.Port;
            string username = SettingsModule.Ssh.Username;
            string password = SettingsModule.Ssh.Password;
            ConnectionDetails = new ConnectionInfo(ipAddress, port, username, new PasswordAuthenticationMethod(username, password));
            SshClientConnection = new SshClient(ConnectionDetails);
            SshClientConnection.Connect();
        }

        private bool IsConnected()
        {
            if (SshClientConnection == null)
                return false;
            else
                return SshClientConnection.IsConnected;
        }

        public string RunCommand(string command)
        {
            if (!IsConnected()) ConnectToServer();
            using (var sshCommand = SshClientConnection.CreateCommand(command))
            {
                return sshCommand.Execute();
            }
        }

        public void StartInfiniteCommand(string command)
        {
            if (!IsConnected()) ConnectToServer();
            if (IsRunningInfinite)
            {
                StopInfiniteCommand();
            }
            IsRunningInfinite = true;
            SshRunningCommand = SshClientConnection.CreateCommand(command);
            AsyncSshThread = new Thread(PingRunningThread)
            {
                IsBackground = true
            };
            AsyncSshThread.Start();
        }

        private void PingRunningThread()
        {
            IAsyncResult result = SshRunningCommand.BeginExecute();

            using (var reader = new StreamReader(SshRunningCommand.OutputStream, Encoding.UTF8, true, 1024, true))
            {
                while (!result.IsCompleted || !reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line != null)
                    {
                        ResponseReached(new ResponseEventArgs(line));
                    }
                    Thread.Sleep(1); // oczekiwanie 1 ms aby nie wzrosło obiciążenie procesora do 100%
                }
                SshRunningCommand.EndExecute(result);
            }
            //SshClientConnection.Disconnect();
        }

        public void AddEventListener(EventHandler<ResponseEventArgs> eventHandler)
        {
            ResponseEvent += eventHandler;
        }

        public void RemoveEventListener(EventHandler<ResponseEventArgs> eventHandler)
        {
            ResponseEvent -= eventHandler;
        }

        public void StopInfiniteCommand()
        {
            SshRunningCommand?.CancelAsync();
            AsyncSshThread?.Join(1000); //HACK: Wątek powinnien automatycznie się kończyć a nie być zabijany.
            IsRunningInfinite = false;
        }

        protected virtual void ResponseReached(ResponseEventArgs e)
        {
            ResponseEvent?.Invoke(this, e);
        }

    }

    public class ResponseEventArgs : EventArgs
    {
        public ResponseEventArgs(string result)
        {
            CommandResult = result;
        }
        public string CommandResult { get; set; }
    }

    public static class NmapXml
    {
        public static ObservableCollection<ServerPortInfo> PortCheckToList(string xmlString)
        {
            ObservableCollection<ServerPortInfo> ListPorts = new ObservableCollection<ServerPortInfo>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);
            XmlNodeList portsXml = doc.GetElementsByTagName("port");
            foreach (XmlNode onePort in portsXml)
            {
                ListPorts.Add(new ServerPortInfo
                {
                    PortNumber = onePort.Attributes["portid"].Value,
                    PortState = onePort.ChildNodes[0].Attributes["state"].Value,
                    PortName = onePort.ChildNodes[1].Attributes["name"].Value
                });
                //Debug.WriteLine(test.Attributes["portid"].Value);
                //Debug.WriteLine(test.ChildNodes[0].Attributes["state"].Value);
                //Debug.WriteLine(test.ChildNodes[1].Attributes["name"].Value);
                //Debug.WriteLine("");
            }

            return ListPorts;
        }
    }
    public class ServerPortInfo
    {
        public string PortNumber { get; set; }
        public string PortState { get; set; }
        public string PortName { get; set; }
    }
}